
# DeepOC
  
To test around the tool, make sure you installed it already. If not, please execute the following command:

    pip install deepoc

Clone the examples:

	git clone git@bitbucket.org:biomodels/deepoc.git

Go to examples folder:

    cd examples
 
There are some few tests for each of our features that you can try. For example, to test only DNN training:

    python test_dnn_training.py

Or, to test everything:

    python test_all.py
