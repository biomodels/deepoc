import csv
import json
import logging
import os
import shutil

import deepoc
from deepoc import DeepOCClassifier

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("tensorflow").setLevel(logging.ERROR)
logger = logging.getLogger(__name__)

train_file = os.path.join('dataset', 'training.csv')
test_file = os.path.join('dataset', 'testing.csv')
val_file = os.path.join('dataset', 'validating.csv')
class_file = os.path.join('dataset', 'classes.txt')
workspace = 'model'

logger.info("Loading ground truth data...")
with open('ground_truth.json') as f:
    ground_truth = json.load(f)

with open(class_file) as f:
    content = f.readlines()
    classes = [x.strip() for x in content]

logger.info("Building features list")
features = deepoc.build_features(ground_truth)

# Picking the first 300 features
selected_features = [feature['feature'] for idx, feature in enumerate(features) if idx < 300]

logger.info("Features: %s", selected_features)

train, test, val = deepoc.generate_dataset(ground_truth, selected_features, classes)

# Writing dataset to file
deepoc.write_dataset_to_file(train, train_file)
deepoc.write_dataset_to_file(test, test_file)
deepoc.write_dataset_to_file(val, val_file)

if os.path.isdir(workspace):
    shutil.rmtree(workspace)

classifier = DeepOCClassifier(workspace, 'GD', [150], 0.001, train_file, test_file, classes, 0.5)
classifier.train_dll_model(1000, 10, 16)

corrected = 0
total = 0
with open(val_file) as csv_file:
    csv_reader = csv.DictReader(csv_file)
    for row in csv_reader:
        model_id = row['model']
        predict_result = classifier.predict(row)
        if len(predict_result) > 0:
            logger.info('Model %s: %f', model_id, max(predict_result[0]['probabilities']))
            if int(predict_result[0]['class_ids'][0]) == int(row['class']):
                corrected += 1
        total += 1
    logger.info("Predict corrected: %d/%d", corrected, total)

