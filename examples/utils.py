def conversion(sec):
    sec_value = sec % (24 * 3600)
    hour_value = sec_value // 3600
    sec_value %= 3600
    min = sec_value // 60
    sec_value %= 60
    return "{} hour(s) {} minute(s) {} second(s)".format(hour_value, min, sec_value)


if __name__ == "__main__":
    sec = 456
    print(conversion(sec))
